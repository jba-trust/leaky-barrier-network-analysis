function stream_network()
% 14 Oct 2019 - taken from stream_network to provide an example for penny
% gill
% penny_gill6 is to run specific selections of dams on 20 segments

fn = mfilename;

% load penny gill data
dd = penny_gill_data_v2;

% resample
dd0 = dd;
N_segs = 20;
dd.x = linspace(dd0.x(1),dd0.x(end),N_segs+2)';
dd.s = interp1(dd0.x,dd0.s,dd.x);
dd.w = interp1(dd0.x,dd0.w,dd.x);

% assign dams
dd.b = 1e9*ones(N_segs+2,1);
dd.H = 1e9*ones(N_segs+2,1);
id = [2 14 15 16 17 19 20 21]; % edges to which dams assigned
dd.b(id) = 0.02; 
dd.H(id) = 1;

% figure(1); clf; plot(dd.x,dd.s,'.-','linewidth',1.5); xlabel('Distance [m]'); ylabel('Elevation [m]');
% figure(2); clf; plot(dd.t,dd.q,'.-','linewidth',1.5); xlabel('Time [h]'); ylabel('Flow [m^3/s]');

% parameters and scales (to make non-dimensional)
pp.Q = .1; % dischage m^3/s
pp.w = 2; % width m
pp.l = 100; % length m
pp.n = 0.1; % Manning roughness s/m^(1/3)
pp.S = .1; % slope
pp.h = (pp.Q/pp.w/pp.S^(1/2)*pp.n)^(3/5); % water depth m
pp.A = pp.h*pp.w;   % cross-sectional area m^2
pp.t = pp.A*pp.l/pp.Q; % time s
pp.th = 60*60; % hour s
pp.g = 9.8; % gravity m^2/s
pp.gamma = (2*pp.g)^(1/2)*pp.w*pp.h^(3/2)/pp.Q;
pp.lambda = 1; % 20; % enhancement factor
pp.beta = pp.lambda*pp.h/pp.S/pp.l;
pp.alpha = pp.h/pp.w;

pp,

% parameters
N = length(dd.x)-2;              % number of stream segments
ad = sparse(1:N-1,2:N,1,N,N,N); % adjacency matrix (describes the network structure)
S = -(dd.s(2:end-1)-dd.s(1:end-2))./(dd.x(2:end-1)-dd.x(1:end-2))/pp.S;    % slopes
w = dd.w(2:end-1)/pp.w;    % widths
l = (dd.x(2:end-1)-dd.x(1:end-2))/pp.l;    % lengths
b = dd.b(2:end-1)/pp.h;  % dam bottoms (make large for no dam)
H = dd.H(2:end-1)/pp.h;  % dam tops 
k = 0*ones(N,1);    % dam permeability
n = 1*ones(N,1);    % dimensionless Manning roughness

% % estimate stream depths at max flow and set dam heights to reflect these
% Qmax = max(dd.q)/pp.Q;
% hmax = (Qmax.*n./w./S.^(1/2)).^(3/5),
% b(b<1e6) = 0.15*hmax(b<1e6);
% dd.b(2:end-1) = pp.h*b;

b2 = b; b2(b>1e8) = NaN; b2,
H2 = H; H2(H>1e8) = NaN; H2,

% non-dimensional flux and area functions
Q_fun = @(h,w,l,S,n,b,H,k) ( w.^(5/3).*h.^(5/3).*(w+2*pp.alpha*h).^(-2/3).*S.^(1/2)./n ).*(h<b) ...
    + min( pp.gamma*(w.*b.*h.*(h+b).^(-1/2)+w.*k.*h.^(1/2).*(h-b)) , w.^(5/3).*h.^(5/3).*(w+2*pp.alpha*h).^(-2/3).*S.^(1/2) ).*((H>h)&(h>=b)) + ...
     + min( pp.gamma*(w.*b.*h.*(h+b).^(-1/2)+w.*2/3.*(h-H).^(3/2)+w.*k.*h.^(1/2).*(H-b)) , w.^(5/3).*h.^(5/3).*(w+2*pp.alpha*h).^(-2/3).*S.^(1/2) ).*(h>=H); 
h_fun = @(A,w,l,S,b,H) ( A./w ).*(A<w.*b) ...
    + ( b + (2/pp.beta*(A-w.*b).*S.*l.*w.^(-1)).^(1/2) ).*(A>=w.*b);

% % plot relationship between h and Q
h = linspace(0,3,100);
% figure(3); clf; plot(h,Q_fun(h,w(1),l(1),S(1),n(1),b(1),H(1),k(1)),'-'); return;
% figure(3); clf; plot(h,Q_fun(h,w,l,S,n,b,H,k),'-'); return;
A = linspace(0,5,100);
% figure(4); clf; plot(A,h_fun(A,w,l,S,b,H),'-'); return;

% flood properties
t_max = dd.t(end)*pp.th/pp.t;
Q_in = @(t) 1*interp1( dd.t*pp.th/pp.t , dd.q/pp.Q , mod(t,t_max) );

% solve odes
opts = odeset('reltol',1e-8,'abstol',1e-7);  % solver options
q_fun = @(t) [1; zeros(N-1,1)]*Q_in(0)*t.^0; % base state inflow
[tt,AA] = ode15s(@odes3,[0 24*pp.th/pp.t],1e-6*ones(N,1)); % run for one day to reach approximate steady initial state
q_fun = @(t) [1; zeros(N-1,1)]*Q_in(t); % flood 
[tt,AA] = ode15s(@odes3,[0:1*pp.th/60/pp.t:t_max],AA(end,:),opts);  % run     

% plot solutions
for it = 1:length(AA)
    hh(it,:) = h_fun(AA(it,:)',w,l,S,b,H)';
    QQ(it,:) = Q_fun(hh(it,:)',w,l,S,n,b,H,k)';
end
Q0 = sum(q_fun(tt'));
Qout = QQ(:,end);

[Q_max,tmp] = max(QQ(:,end));
t_max = tt(tmp);

% dimensional plots of solutions
figure(2); clf; set(gcf,'units','centimeters','paperpositionmode','auto','position',[4 4 16 16]);
ax1 = subplot(2,1,1);
    plot(pp.t/pp.th*tt',pp.Q*Q0,'b--','linewidth',2);
    hold on;
    plot(pp.t/pp.th*tt',pp.Q*QQ(:,end),'b-','linewidth',2);
    xlabel('Time t [h]');
    ylabel('Discharge Q [m^3/s]');
ax2 = subplot(2,1,2);
    plot(pp.t/pp.th*tt,sum(pp.A*pp.l*AA.*(tt.^0*l'),2),'b-','linewidth',2);
    hold on; 
    axis(axis);
    xlabel('Time t [h]');
    ylabel('Volume V [m^3]');
    
% print(gcf,'-depsc2',[fn,'_fig2']);
% print(gcf,'-dpng',[fn,'_fig2']);


% dimensional plots of solutions
figure(3); clf; set(gcf,'units','centimeters','paperpositionmode','auto','position',[4 4 16 16]);
id = 1:N;
ax1 = subplot(3,1,1);
    plot(pp.t/pp.th*tt,pp.Q*QQ(:,id),'-');
    hold on;
    plot(pp.t/pp.th*tt',pp.Q*Q0,'k--','linewidth',2);
    plot(pp.t/pp.th*tt,pp.Q*QQ(:,end),'k-','linewidth',2);
    xlabel('Time t [h]');
    ylabel('Discharge Q [m^3/s]');
ax2 = subplot(3,1,2);
    plot(pp.t/pp.th*tt,pp.h*hh(:,id),'-');
    hold on; 
    axis(axis);
    ax = gca; ax.ColorOrderIndex = 1;
    plot(pp.t/pp.th*tt,pp.h*tt.^0*b(id)','k--'); 
    ax = gca; ax.ColorOrderIndex = 1;
    plot(pp.t/pp.th*tt,pp.h*tt.^0*H(id)','k--');
    xlabel('Time t [h]');
    ylabel('Depth h [m]');
ax3 = subplot(3,1,3);
    plot(pp.t/pp.th*tt,pp.A*pp.l*AA(:,id).*(tt.^0*l(id)'),'-');
    hold on; 
    axis(axis);
    xlabel('Time t [h]');
    ylabel('Volume V [m^3]');
axes(ax2); 
    tmp = legend(num2str((N:-1:1)')); tmp.FontSize = 6;
    
print(gcf,'-depsc2',[fn,'_fig3']);
% print(gcf,'-dpng',[fn,'_fig3']);
     
% plot sideview
figure(4); clf; set(gcf,'units','centimeters','paperpositionmode','auto','position',[20 4 20 16]);
for it = 1:30:length(AA)
    offset = it/10;
    plot_sideview(AA(it,:)',offset);
    text(dd.x(end)+10,dd.s(end)-offset,['t = ',num2str(pp.t*tt(it)/pp.th,2),'h'],'horizontalalignment','left','verticalalignment','middle');
end
box off;
xlabel('Distance [m]');
ylabel('Elevation - offset [m]');

maxQ0 = max(pp.Q*Q0);
maxQ = max(pp.Q*QQ(:,end));
maxV = max(sum(pp.A*pp.l*AA.*(tt.^0*l'),2));

maxQ/maxQ0,
maxV,

text(0.1,0.15,['Max Q reduction ' num2str(maxQ/maxQ0,3)],'units','normalized');
text(0.1,0.1,['Max V ' num2str(maxV,3),' m^3'],'units','normalized');

% % add inset with discharge hydrographs
% ax = axes('position',[0.5 0.75 0.45 0.2]);
%     plot(pp.t/pp.th*tt,pp.Q*QQ(:,id),'-','color',0.8*[1 1 1]);
%     hold on;
%     plot(pp.t/pp.th*tt',pp.Q*Q0,'k--','linewidth',2);
%     plot(pp.t/pp.th*tt,pp.Q*QQ(:,end),'k-','linewidth',2);
%     xlabel('Time t [h]');
%     ylabel('Discharge Q [m^3/s]');

% print(gcf,'-depsc2',[fn,'_fig4']);
print(gcf,'-dpng',[fn,'_fig4']);


% save
save(fn);



function plot_sideview(A,offset)
    h = h_fun(A,w,l,S,b,H);
    b2 = b; b2(b>1e6) = NaN;
    H2 = H; H2(H>1e6) = NaN;
    plot(dd.x,dd.s-offset,'k-','linewidth',1); % bed elevation
    hold on;
    tmp = area(reshape([dd.x(1:end-2) dd.x(2:end-1)]',[],1),reshape([dd.s(2:end-1)+pp.h*h dd.s(2:end-1)+pp.h*h]',[],1)-offset,min(dd.s)-offset,'facecolor','b','linestyle','none'); tmp.BaseLine.Visible = 'off'; % water level
%     plot(reshape([dd.x(1:end-2) dd.x(2:end-1)]',[],1),reshape([dd.s(2:end-1)+pp.h*h dd.s(2:end-1)+pp.h*h]',[],1),'b-','linewidth',1.5); % water level
    tmp = area(dd.x,dd.s-offset,min(dd.s)-offset,'facecolor','w','linestyle','none'); tmp.BaseLine.Visible = 'off'; % bed elevation
    plot(dd.x,dd.s-offset,'k-','linewidth',1); % bed elevation
    plot((dd.x(2:end-1)*[1 1])',([dd.s(2:end-1)+pp.h*b2 dd.s(2:end-1)+pp.h*H2])'-offset,'k-','linewidth',3);  % dams
    set(gca,'Layer','top');
end

% network ode functions   
function dydt = odes3(t,y)
    A = y;
    h = h_fun(A,w,l,S,b,H);
    Q = Q_fun(h,w,l,S,n,b,H,k);
    q = q_fun(t);
    dydt = l.^(-1).*(ad'*Q - Q + q);
end

end





