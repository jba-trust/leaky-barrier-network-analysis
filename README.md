# leaky barrier network analysis

Source code and data to support analysis described in the paper "A risk-based network analysis of distributed in-stream leaky barriers for flood risk management" by Hankin et al.,  Nat. Hazards Earth Syst. Sci., 20, 1–17, 2020 (https://doi.org/10.5194/nhess-20-1-2020)